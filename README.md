# vagrant_crc_splunk

Requirements:

Download `splunk-7.2.3-06d57c595b80-Linux-x86_64.tgz` and place into ansible/tar

`vagrant up test01 --provision` from root folder 
- brings up test01
- runs THP role on test01
- runs ulimit role on test01



`vagrant up splunk --provision` from root folder
- bring up Splunk 
- Splunk GUI here is `splunk:8000`