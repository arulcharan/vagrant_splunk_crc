# vagrant specific variables
VAGRANT_API_VERSION = "2"

#BENTO_CENTOS_BOX = "bento/centos-7.5"
#BENTO_CENTOS_BOX_VERSION = "201808.24.0"
BENTO_CENTOS_BOX = "bento/centos-7.6"
BENTO_CENTOS_BOX_VERSION = "201812.27.0"


HOST_DOMAIN="vagrant.dev"
# ram and cpu allocation for candidate hosts
HOST_MEM_HIGH_VAL=3072
HOST_MEM_MID_VAL=2048
HOST_MEM_LOW_VAL=1024
HOST_CPU_HIGH_VAL=4
HOST_CPU_LOW_VAL=1

# http://nifi:8080/nifi/
TEST01_HOSTNAME="test01"
SPLUNK_HOSTNAME="splunk"

ANSIBLE_VERBOSE_LEVEL=""

# declare all hosts within splunk_cluster
splunk_cluster = [
    { :hostname => "#{SPLUNK_HOSTNAME}", :ip => "192.168.100.101", :box => "#{BENTO_CENTOS_BOX}", :version =>"#{BENTO_CENTOS_BOX_VERSION}",:ram => "#{HOST_MEM_MID_VAL}", :cpu =>"#{HOST_CPU_HIGH_VAL}", :gui => false },
    { :hostname => "#{TEST01_HOSTNAME}", :ip => "192.168.100.100", :box => "#{BENTO_CENTOS_BOX}", :version =>"#{BENTO_CENTOS_BOX_VERSION}",:ram => "#{HOST_MEM_HIGH_VAL}", :cpu =>"#{HOST_CPU_HIGH_VAL}", :gui => false },
]

ETC_HOSTS_SPLUNK_CL = ""
splunk_cluster.each do |n|
    ETC_HOSTS_SPLUNK_CL << "#{n[:ip]} #{n[:hostname]} #{n[:hostname]}\n"
end
puts ""
puts "===========================================================================================\n"
puts "\t SPLUNK CLUSTER\n"
puts ETC_HOSTS_SPLUNK_CL

$ETC_HOST_SCRIPT = <<SCRIPT
#!/bin/bash
cat > /etc/hosts <<EOF
127.0.0.1 localhost localhost.localdomain
#{ETC_HOSTS_SPLUNK_CL}
EOF

hostname --fqdn > /etc/hostname && hostname -F /etc/hostname

sed "s/^[ \t]*//" -i /etc/hosts

cat /etc/hosts
SCRIPT

Vagrant.configure(VAGRANT_API_VERSION) do |config|
    # disable box udpate on import
    config.vm.box_check_update = false
    # hostmanager config
    config.hostmanager.enabled = true
    config.hostmanager.manage_host = true
    config.hostmanager.manage_guest = false
    config.hostmanager.ignore_private_ip = false
    config.hostmanager.include_offline = true

    # splunk cluster
    splunk_cluster.each do |host|

        config.vm.define host[:hostname] do |host_config|

            host_config.vm.box = host[:box]
            host_config.vm.box_version = host[:version]
            host_config.vm.network "private_network", ip: host[:ip]
            host_config.vm.hostname = "#{host[:hostname]}.#{HOST_DOMAIN}"
            host_config.hostmanager.aliases = "#{host[:hostname]}"

            host_config.vm.provider :virtualbox do |v|
                v.name = host[:hostname].to_s
                v.gui = host[:gui]
                v.customize ["modifyvm", :id, "--memory", host[:ram].to_s ]
                v.customize ["modifyvm", :id, "--cpus", host[:cpu].to_s ]
            end
            host_config.vm.provision :shell, :inline => $ETC_HOST_SCRIPT

            # ansible provision: stands up splunk cluster
            host_config.vm.provision :ansible_local do |ansible|
                ansible.groups = {
                    "test01_servers" => [
                        "test01"
                    ],
                }
                if host[:hostname] == "#{SPLUNK_HOSTNAME}"
                    ansible.playbook = "ansible/splunk-server.yml"
                elsif host[:hostname] == "#{TEST01_HOSTNAME}"
                    ansible.playbook = "ansible/test01-server.yml"
                end
                # ansible verbose level
                ansible.verbose = "#{ANSIBLE_VERBOSE_LEVEL}"
            end
        end
    end
end


